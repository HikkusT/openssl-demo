# OpenSSL demonstration

Simple demonstration code using libcrypt from OpenSSL. Presentation slides  in
Portuguese.

_Disclaimer: there's no guarantee of security using this program, use if just for
study!_

## Dependencies:

- Just OpenSSL development files
- On Arch Linux:
	- `sudo pacman -S openssl`
- On Debian/Ubuntu:
	- `sudo apt install openssl libssl-dev`

- Python packages
	- `pip install pyopenssl`
	- `pip install cryptography`

## Using with C source files

After you have all dependencies, you can compile with and test with:

```shell
$ make
$ make test
```

- To generate a key:

```shell
$ ./gen_key <keyfile>
```

- To encrypt some file:

```shell
$ ./filencrypt <keyfile> -e <plain_file>
```

- To decrypt some file:

```shell
$ ./filencrypt <keyfile> -d <encrypted_file>
```

## Using with Python scripts

- To generate a key:

```shell
$ python3 gen_key.py <keyfile>
```

- To encrypt some file:

```shell
$ python3 filencrypt.py <keyfile> -e <plain_file>
```

- To decrypt some file:

```shell
$ python3 filencrypt.py <keyfile> -d <encrypted_file>
```

## License

[This project is licensed under the GNU General Public License v2.0.](LICENSE.md)
