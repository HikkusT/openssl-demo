import os
import sys
import binascii
from OpenSSL._util import (
    lib as ssl_lib
)

KEY_LEN = 256//8 # 256 bits key = 32 bytes key
SEED_LEN = 32 # length of RNG seed

# Do a basic check at user input
def check_input():
    if len(sys.argv) != 2:
        print("Usage: python3 gen-key_py.py <keyfile>")
        quit()
    
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print("Usage: python3 gen-key_py.py <keyfile>")
        quit()

# Ressed the OpenSSL RNG with a fresh seed
# This isn't really necessary since OpenSSL will do it automatically
def reseed_RNG():
    num_bytes = ssl_lib.RAND_load_file('/dev/random'.encode('ascii'), SEED_LEN)
    print('Read', num_bytes, 'bytes from /dev/random')

    if num_bytes != SEED_LEN:
        print('ERROR: Could not get enough random bytes.')
        quit()


################ The actual script ################

check_input()
reseed_RNG()

# Key Generation
aes_key = os.urandom(KEY_LEN)

# Never print this in production
print(binascii.hexlify(aes_key))

# Write key to file
with open(sys.argv[1], 'wb') as file:
    file.write(aes_key)

# Replace the key in memory
aes_key = os.urandom(KEY_LEN)