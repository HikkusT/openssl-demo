# Programando aplicações criptográficas com OpenSSL
## `ENIGMA`

---

# Aplicações criptográficas (ou que usam criptografia):

---

# Aplicações criptográficas (ou que usam criptografia):

- Navegadores (Firefox, Tor Browser, Chrome)
- Mensageiros (Telegram, WhatsApp)
- Criptografia de email (OpenPGP)
- Criptografia de disco (LUKS, VeraCrypt)

---

# Aplicações criptográficas (ou que usam criptografia):

- Navegadores (Firefox, Chrome)
- Mensageiros (Telegram, WhatsApp)
- Criptografia de email (OpenPGP)

_No geral, programas que se comunicam_

---

# Quero escrever meu próprio programa para..

- Me comunicar seguramente
- Navegar anonimamente
- Proteger meus arquivos

---

# Quero escrever meu próprio programa para..

- Me comunicar seguramente
- Navegar anonimamente
- Proteger meus arquivos

_Tem certeza?_

---

# Quero escrever meu próprio programa para..

- Desenvolvimento seguro é uma habilidade muito difícil e que exige muito estudo
- Lembre-se: nao exite nada 100% seguro nem livre de bugs
- Analise seu cenário de risco: provavelmente é melhor usar algo com uma diversidade de desenvolvedores e vários bugs resolvidos

---

# Quero escrever meu próprio programa para..

- Mas vai fundo: aprender é sempre incrível!
- Voce pode aprender para poder contribuir com projetos open source
- Quem sabe um dia comecar um projeto seu

---

# Algoritmos criptográficos

- Geralmente não é uma boa ideia escrever seu próprio algoritmo
- Algoritmos seguros
	- > Anyone, from the most clueless amateur to the best cryptographer, can create an algorithm that he himself can't break. 	
	- A segurança de um algoritmo não está na sua obscuridade.

---

# Bibliotecas criptográficas

- Da mesma forma que escrever aplicações seguras é difícil, bibliotecas criptográficas tambem são
- Processadores recentes disponibilizam instrucoes para criptografia (AES-NI)
- O kernel te prove uma API para usa-las

---

# Bibliotecas criptográficas

- Algoritmo seguro vs implementação segura
- Operacoes criptográficas precisam ser muito eficientes
- Gerenciamento de memória preciso: apagar dados sensiveis da memória
- Linguagem C cabe nesses dois requisitos
- Contudo, C tambem é muito perigoso

---

# Boas praticas em C

- Validação da entrada
- Verificação de tamanhos
- "Higienize" a saida
- Limpeza das variaveis
- Checagem de erros
- Separação de codigos com privilegios diferentes
- Mantenha simples

---

# SSL da Apple

```c
static OSStatus
SSLVerifySignedServerKeyExchange(SSLContext *ctx, bool isRsa, SSLBuffer signedParams,
                                 uint8_t *signature, UInt16 signatureLen)
{
	OSStatus        err;
	...

	if ((err = SSLHashSHA1.update(&hashCtx, &serverRandom)) != 0)
		goto fail;
	if ((err = SSLHashSHA1.update(&hashCtx, &signedParams)) != 0)
		goto fail;
		goto fail;
	if ((err = SSLHashSHA1.final(&hashCtx, &hashOut)) != 0)
		goto fail;
	...

fail:
	SSLFreeBuffer(&signedHashes);
	SSLFreeBuffer(&hashCtx);
	return err;
}
```

---

# Heartbleed

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Simplified_Heartbleed_explanation.svg/1024px-Simplified_Heartbleed_explanation.svg.png)

---

# Sites afetados

- Google, YouTube, Gmail
- Facebook, Instagram
- Linkedin, Amazon, Dropbox

---

# OpenSSL

- Uma das implementacoes mais famosas e usadas hoje em dia, escrita em C
- Permite integração para acelaração de hardware e com padroes e certificacoes de seguraca
- Fornece um "front-end" para as chamadas ao kernel
- OpenSSL prove tres coisas:
- `openssl-cli`: aplicação de linha de comando para realizar acoes criptográficas (e.g. gerar certificado, criar par de chaves, cifrar arquivos, etc)
- `libssl`: biblioteca para prover criptografia em rede, implementando o procotolo TLS/SSL
- `libcrypt`: prove operacoes criptográficas

---

# Exemplo

- No nosso caso, vamos escrever uma aplicação em C com uma operação de criptografia simetrica
- Precisamos saber o seguinte:
	- Algoritmo e modo a ser utilizado
	- Chave e vetor de inicializao

---

# Exemplo

- Algoritmo criptografico (ex AES, RSA)
- Modo do algoritmo (ECB, CBC)
- Chave criptográfica da mensagem
- Vetor de inicializao: parametro utilizado individualmente por cada mensagem cifrada, garante que mensagens iguais sejam cifradas de forma diferente

---

# Vetor de inicialização

- Alice: DCF0C50A96DC2D9B05F2AC4C24CB9B93
- Bob: **E0B554B341FF5632DE241FBF4B1DBB37**
- Alice: 6742FA9512C8C2ACE6942974C8C848FC
- Bob: _824783C3B272FF7129F9E153EC10D1AE_
- Alice: C90BD345639368D951A8B5E267427514
- Bob: **E0B554B341FF5632DE241FBF4B1DBB37**
- Alice: AC797939F55E87C361A8F02B4CEA1A08
- Bob: _824783C3B272FF7129F9E153EC10D1AE_
 
- Are you Bob?
- Are you Eve?
- Are you Smart?
- Are you Tired?

---

# Programando do jeitinho OpenSSL

- No OpenSSL, trabalhamos com "contexto criptografico"
- Uma estrutura `EVP_CIPHER_CTX` guarda tudo sobre a operação de cifragem
	- Inicializamos o contexto
	- Atualizamos o contexto
	- Finalizamos o contexto
- `if(função() != 1) trata_erro();`
	- Tudo é passível de erro!

---

# Forks do OpenSSL

- LibreSSL
- BoringSSL

---

# Olhando o código
